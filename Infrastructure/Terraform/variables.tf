variable "aws_region" {default = "ap-southeast-1"}
variable "cred_file" { default = "/var/lib/jenkins/.aws/credentials" }
variable "key_name"  {default = "sin-pair"}
variable "vpc_id"  {default = "vpc-c87044af"}
variable "subnet1"  {default = "subnet-1c332a7b"}
variable "subnet2"  {default = "subnet-74af7b2d"}
variable "subnet3"  {default = "subnet-ff4d44b6"}
